 # SOLID :
 * Acronym for five design principles
 * Make software designs understandable, flexible and maintainable.
 * Developers should have knowledge of SOLID principles.

 ---

 # PRINCIPLES :
- S -->  Single Responsibility Principle.
- O -->  Open/Closed Principle.
- L -->  Liskov’s Substitution Principle.
- I -->  Interface Segregation Principle.
- D -->  Dependency Inversion Principle.



![SOLID PRINCIPLES](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20191012234920/SOLID-Principle-in-Programming-Understand-With-Real-Life-Examples.png)



---



 ## 1. Single Responsibility Principle

- States that every class should have a single task/purpose rather than one class having many task/purpose.
- Let's consider an example - 
```
class Medicine
{
    constructor(Name, Price)
    {
        this.name = Name;
        this.price = Price;
    }

    tabName()
    {
        return `Tablet name is ${this.name}`
    }


    tabPrice()
    {
        return `Tablet price is ${this.price}`
    }
}

```

Above code has one class *__Medicine__* and it has two function *__tabName()__* and  *__tabPrice()__*. Thus one class performing two tasks and violating the very first principle.

```
class Medicine
{
    constructor(Name)
    {
        this.name = Name;
    }

    tabName()
    {
        return `Tablet name is ${this.name}`
    }
}

```

Now in this code, one class is doing one job and this following the first priciple.



 ## 2. Open/Closed Principle

- States that "software entities (classes, functions) should be open for extension, but closed for modification".
- Suppose one person made a class *__Medicine__* that display name of tablets. Now second person wants to add a feature which display price of medicine. See below example to know what second person should do.

Initially my code is this:

```
class Medicine
{
    constructor(Name, Price)
    {
        this.name = Name;
        this.price = Price;
    }

    tabName()
    {
        return `Tablet name is ${this.name}`
    }
}

```

To add price feature and at the same time does not violate this principle, second person can do this:

```
class Medicine
{
    constructor(Name, Price)
    {
        this.name = Name;
        this.price = Price;
    }

    tabName()
    {
        return `Tablet name is ${this.name}`
    }


    tabPrice()
    {
        return `Tablet price is ${this.price}`
    }
}

```

Here second person just extend the class rather than modifying the original code. Thus satisfying the Open/Closed Principle.



 ## 3. Liskov’s Substitution Principle


- States that child class of a parent class should be used in place of its parent class without any unexpected behaviour.
- In simple words, this principle ensures that parent class should be easily substituted with their child class without crashing the application.
- Time for example:

Let's consider a __*Medicine*__ parent class.

```
class Medicine
{
    isMed()
    {
        console.log("Yes, it's a medicine");
    }
}

```

Now let's consider the __*Gemer*__ and  *__Saridon*__ classes which extends __*Medicine*__ (parent class).

```
class Gemer extends Medicine
{
    isMed()
    {
        console.log("Yes, it's a medicine");
    }
}

class Saridon extends Medicine
{
    isMed()
    {
        console.log("Yes, it's a medicine");
    }
}

```


In above code, we are able to substitute __*Medicine*__ parent class with __*Gemer*__ or __*Saridon*__ without exploding the code.

- But suppose if this happens:

```
class Petrol extends Medicine
{
    isMed()
    {
        error("No, it's not a medicine");
    }
}
```

By running above code, we can see some unexpected behaviour because petrol is not a medicine.
Thus above one violates Liskov's Principle.



 ## 4. Interface Segregation Principle

- First principle out of above three which applies on interface rather than on classes.
- States that many client-specific interfaces are better than one general interface.
- Similar to Single Responsibility Principle except this one applies on interface.
- This principle says that we should not impose the implementation of something if it's not needed there.
- Example:

```
interface customer
{
    bool signup();
    bool login();
    bool search();
    void about_us(); 
}

```
 
In above code, we have one general interface __*customer*__ rather than many client-specific interfaces. Thus above code violates Interface Segregation Principle.


Below code is correct implementation:

```
interface customer
{
    bool signup();
    bool login();
}

interface forSearch
{
    bool search();
}

interface description
{
    void aboutUs();
}

```



 ## 5. Dependency Inversion Principle


- states two things:
    - High-level modules/classes should not depend upon low-level modules/classes. Both should depend upon abstraction.
    - Abstraction should not depend upon details. Details should depend upon abstraction.
- In brief, if high-level class depend more on low-level class then your code would have tight coupling and if you try making a change in one class, it can break another class.
- Hence this principle follows decoupling dependencies, so if class A changes then class B doesn't need to worry.

```
class Gemer{}

class Saridon{}

class Medicine
{
    constructor()
    {
        this.G = new Gemer();
        this.S = new Saridon();
    }
}

class Random
{
    constructor()
    {
        this.med = new Medicine();
    }
}

```

In this example, class __*Gemer*__ and __*Saridon*__ does not going to break while implementation of __*Random*__ class. As long as __*Medicine*__ class doesn't change, there is no need to worry.



 # REFERENCE LINKS
- [Reference 1](https://www.geeksforgeeks.org/solid-principle-in-programming-understand-with-real-life-examples/)
- [Reference 2](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)
- [Reference 3](https://www.youtube.com/watch?v=yxf2spbpTSw&t=146s)
- [Reference 4](https://www.youtube.com/watch?v=gumM1H4qLUM&t=811s)
- [Reference 5](https://medium.com/mindorks/solid-principles-explained-with-examples-79d1ce114ace) 
